package com.hsbc.threads;

public class PhotoSharingApplication {
	public static void main(String[] args) {
		Runnable googlePhotos = new GooglePhotos();
		Runnable flickr = new Flickr();
		Runnable picassa = new Picassa();
		
		Thread google = new Thread (googlePhotos);
		google.setName("Google-Photos");
		
		Thread flickr1 = new Thread (flickr);
		flickr1.setName("Flickr");
		
		Thread picassa1 = new Thread (picassa);
		picassa1.setName("Picassa");
		
		google.start();
		flickr1.start();
		picassa1.start();
		
		try {
			google.join();
			flickr1.join();
			picassa1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("All Items are delivered");
	}
}

class GooglePhotos implements Runnable {

	@Override
	public void run() {
		System.out.println("============ Thread "+ Thread.currentThread()+ " start ===========");
		
		for(int i = 0 ;i < 3; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread "+ Thread.currentThread()+ " end ===========");
	}
}

class Flickr implements Runnable {

	@Override
	public void run() {
		System.out.println("============ Thread "+ Thread.currentThread()+ " start ===========");
		
		for(int i = 0 ;i < 3; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread "+ Thread.currentThread()+ " end ===========");
	}
}

class Picassa implements Runnable {

	@Override
	public void run() {
		System.out.println("============ Thread "+ Thread.currentThread()+ " start ===========");
		
		for(int i = 0 ;i < 3; i ++) {
			System.out.println("Thread running "  + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread "+ Thread.currentThread()+ " end ===========");
	}
}

