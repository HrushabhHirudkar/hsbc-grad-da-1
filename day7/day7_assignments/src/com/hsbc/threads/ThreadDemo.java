package com.hsbc.threads;

public class ThreadDemo extends Thread {

	public static void main(String[] args) {
	
		Thread mainThread = new Thread();
		mainThread.setName("MainThread");
		Thread childThread = new ChildThread();
		childThread.setName("ChildThread");
		
		mainThread.start();
		childThread.start();
		
		try {
			childThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void run() {
	
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

class ChildThread extends ThreadDemo {
	@Override
	public void run() {
		System.out.println("============ Thread " + Thread.currentThread() + " start ===========");

		for (int i = 0; i < 5; i++) {
			System.out.println("Thread running " + i + " name: " + Thread.currentThread());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("============ Thread " + Thread.currentThread() + " end ===========");
	}
}
