package com.hsbc.da1.thread;

public class ThreadDemo {
	public static void main(String[] args) {
		
		Thread t = Thread.currentThread();
		
		System.out.println("Current Thread is : "+t.getName());
		
		try {
			t.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i=0;i<3;i++) {
			System.out.println("The name of thread is : "+t.getName());
			System.out.println("The state of thread is : "+t.getState().name());
			
			try {
				t.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("Thread is interrupted while in sleep");
			}
			finally {
				System.out.println("The state of thread in finally block is : "+t.getState().name());
			}
		}
		
		System.out.println("Current Thread is : "+t.getName());
	}
}
