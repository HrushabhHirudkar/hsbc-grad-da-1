package com.hsbc.da1.exceptions;

public class ExceptionDemo {
	
	public static void main(String[] args) {
		
		int a=10;
		int b=0;
		
		int array[] = {10,2,30,40};
		
		BaseClass obj = new DerivedClass();
		
		DerivedClass derived = (DerivedClass) obj;

		
		try {
			System.out.println("Result of Division : "+(b/a));
			System.out.println("The last element : "+array[array.length]);
		}
		catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
			
			System.out.println("It is of Arithmetic type "+(e instanceof ArithmeticException));
			System.out.println("It is of ArrayIndexOutOfBoundsException type "+(e instanceof ArrayIndexOutOfBoundsException));
			System.out.println(" Please enter a valid value for division "+ b +" is not allowed here ");
		} catch (ClassCastException e) {
			System.out.println("Cannot downcast with incompatible types");
		} catch(Exception e ) {
			System.out.println("Generic exception case");
		} catch (Throwable e){
			System.out.println("Throwable exceptin "+ e);
		}
		finally {
			System.out.println("This line will be printed if there is no exception");	
		}

	}
}

class BaseClass {
	
}

class DerivedClass extends BaseClass{
	
}

class AnotherDerivedClass extends BaseClass{
	
}

