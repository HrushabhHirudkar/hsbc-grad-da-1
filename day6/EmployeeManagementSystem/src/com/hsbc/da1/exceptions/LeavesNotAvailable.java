package com.hsbc.da1.exceptions;

public class LeavesNotAvailable extends Exception{
	
	public LeavesNotAvailable(String message) {
		super(message);
	}
	
	@Override
	public String getMessage() {
		return super.getMessage();
	}

}
