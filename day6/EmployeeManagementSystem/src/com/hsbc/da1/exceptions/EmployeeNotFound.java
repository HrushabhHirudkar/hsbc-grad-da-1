package com.hsbc.da1.exceptions;

public class EmployeeNotFound extends Exception{

	
	public EmployeeNotFound(String mesage)
	{
		super(mesage);
	}
	
	@Override
	public String getMessage() {
		return super.getMessage();
	}
}
