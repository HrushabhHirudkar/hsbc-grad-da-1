package com.hsbc.da1.client;

import java.util.*;

import com.hsbc.da1.controller.EmployeeManagementController;
import com.hsbc.da1.dao.EmployeeManagementDAO;
import com.hsbc.da1.dao.EmployeeManagementDAOImpl;
import com.hsbc.da1.exceptions.EmployeeNotFound;
import com.hsbc.da1.exceptions.LeavesNotAvailable;
import com.hsbc.da1.model.EmployeeManagement;
import com.hsbc.da1.service.EmployeeManagementService;
import com.hsbc.da1.service.EmployeeManagementServiceImpl;

public class EmployeeManagementClient {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		EmployeeManagementController controller = new EmployeeManagementController();
		EmployeeManagementDAO dao = new EmployeeManagementDAOImpl();
		
		EmployeeManagement aman = controller.addEmployee("Aman", 20, 10000);
		EmployeeManagement raj = controller.addEmployee("Raj", 25, 30000);
		
		List<EmployeeManagement> empList = controller.fetchEmployee();
		
		try {
			System.out.println("Enter Employee ID for application of Leaves");
			int employeeID = input.nextInt();
			System.out.println("Enter the no of days for which you want Leave.");
			int days = input.nextInt();
			
			int daysLeft=controller.applyforleave(employeeID,days);
			System.out.println("Days Left : "+daysLeft);
		} catch (LeavesNotAvailable e) {
			// TODO Auto-generated catch block
			System.out.println("INSUFFICIENT LEAVES");
		}
		catch (EmployeeNotFound emp) {
			System.out.println("Employee not found");
		}
		
		for(EmployeeManagement em : empList) {
			System.out.println("Employee is : "+em);
		}
		
		
		System.out.println("Adding duplicate item Aman\n************************");
		EmployeeManagement amanCopy = new EmployeeManagement("Aman", 20, 10000);
		amanCopy.setEmployeeID(1001);
		dao.addEmployee(amanCopy);
		
		
		for(EmployeeManagement em : empList) {
			System.out.println("Employee is : "+em);
		}
	}

}
