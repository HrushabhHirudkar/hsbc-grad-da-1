package com.hsbc.da1.dao;

import java.util.ArrayList;
import java.util.List;

import com.hsbc.da1.exceptions.EmployeeNotFound;
import com.hsbc.da1.model.EmployeeManagement;

public class EmployeeManagementDAOImpl implements EmployeeManagementDAO{

	private List<EmployeeManagement> employeeList = new ArrayList<EmployeeManagement>();
	@Override
	public EmployeeManagement addEmployee(EmployeeManagement employee) {
		this.employeeList.add(employee);
		return employee;
	}

	@Override
	public EmployeeManagement updateEmployee(EmployeeManagement employee, long employeeID) {
		for (EmployeeManagement em : employeeList) {
			if (em.getEmployeeID() == employeeID) {
				em = employee;
			}
		}
		return employee;
	}

	@Override
	public void deleteEmployee(long employeeID) {
		for (EmployeeManagement em : employeeList) {
			if (em.getEmployeeID() == employeeID) {
				employeeList.remove(em);
			}
		}
	}

	@Override
	public List<EmployeeManagement> fetchEmployee() {
		// TODO Auto-generated method stub
		return this.employeeList;
	}

	@Override
	public EmployeeManagement fetchEmployeeByID(long employeeID) throws EmployeeNotFound{
		for (EmployeeManagement em : employeeList) {
			if (em.getEmployeeID() == employeeID) {
				return em;
			}
		}
		throw new EmployeeNotFound("Employee not found");
	}

}
