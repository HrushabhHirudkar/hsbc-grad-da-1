package com.hsbc.da1.dao;

import java.util.*;

import com.hsbc.da1.exceptions.EmployeeNotFound;
import com.hsbc.da1.model.EmployeeManagement;

public interface EmployeeManagementDAO {
	
	public EmployeeManagement addEmployee(EmployeeManagement employee);
	
	public EmployeeManagement updateEmployee(EmployeeManagement employee,long employeeID);
	
	public void deleteEmployee(long employeeID);
	
	public List<EmployeeManagement> fetchEmployee();
	
	public EmployeeManagement fetchEmployeeByID(long employeeID) throws EmployeeNotFound;
	
}
