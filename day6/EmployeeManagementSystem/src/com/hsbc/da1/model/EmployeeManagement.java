package com.hsbc.da1.model;

public class EmployeeManagement {
	
	private String name;
	private int age;
	private double salary;
	private long employeeID;
	private int leavesLeft;
	private static long employeeCounter = 1000;
	
	public EmployeeManagement(String name,int age,double salary) {
		this.age=age;
		this.leavesLeft=40;
		this.name=name;
		this.salary = salary;
		this.employeeID=++employeeCounter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public long getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(long employeeID) {
		this.employeeID = employeeID;
	}

	public int getLeavesLeft() {
		return leavesLeft;
	}

	public void setLeavesLeft(int leavesLeft) {
		this.leavesLeft = leavesLeft;
	}

	@Override
	public String toString() {
		return "EmployeeManagement [name=" + name + ", age=" + age + ", salary=" + salary + ", employeeID=" + employeeID
				+ ", leavesLeft=" + leavesLeft + "]";
	}
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employeeID ^ (employeeID >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((age == 0 ) ? 0 : (age ^ (age >>> 32)));
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof EmployeeManagement))
			return false;
		EmployeeManagement other = (EmployeeManagement) obj;
		if (employeeID != other.employeeID)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (age == 0) {
			if(other.age!=0)
				return false;
		}
		else if(age!=other.age)
			return false;
		
		return true;
	}
}
