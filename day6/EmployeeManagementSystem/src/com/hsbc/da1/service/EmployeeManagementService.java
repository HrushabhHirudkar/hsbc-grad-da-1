package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.exceptions.EmployeeNotFound;
import com.hsbc.da1.exceptions.LeavesNotAvailable;
import com.hsbc.da1.model.EmployeeManagement;

public interface EmployeeManagementService {

	public EmployeeManagement addEmployee(String name,int age,double salary);

	public void deleteEmployee(long employeeID);

	public List<EmployeeManagement> fetchEmployee();

	public EmployeeManagement fetchEmployeeByID(long employeeID) throws EmployeeNotFound;
	
	public int applyforleave(long employeeID,int noOfDays) throws LeavesNotAvailable, EmployeeNotFound;
	
	public int withdrawLeave(long employeeID,int noOfDays) throws EmployeeNotFound;
}
