package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.dao.EmployeeManagementDAO;
import com.hsbc.da1.dao.EmployeeManagementDAOImpl;
import com.hsbc.da1.exceptions.EmployeeNotFound;
import com.hsbc.da1.exceptions.LeavesNotAvailable;
import com.hsbc.da1.model.EmployeeManagement;

public class EmployeeManagementServiceImpl implements EmployeeManagementService {

	private EmployeeManagementDAO dao = new EmployeeManagementDAOImpl();

	@Override
	public EmployeeManagement addEmployee(String name, int age, double salary) {
		EmployeeManagement employee = new EmployeeManagement(name, age, salary);
		this.dao.addEmployee(employee);
		return employee;
	}

	@Override
	public void deleteEmployee(long employeeID) {
		this.dao.deleteEmployee(employeeID);

	}

	@Override
	public List<EmployeeManagement> fetchEmployee() {
		// TODO Auto-generated method stub
		List<EmployeeManagement> emp = this.dao.fetchEmployee();
		return this.dao.fetchEmployee();
	}

	@Override
	public EmployeeManagement fetchEmployeeByID(long employeeID) throws EmployeeNotFound{
		EmployeeManagement emp = this.dao.fetchEmployeeByID(employeeID);
		return emp;
	}

	@Override
	public int applyforleave(long employeeID, int noOfDays) throws LeavesNotAvailable,EmployeeNotFound {
		int daysLeft = this.dao.fetchEmployeeByID(employeeID).getLeavesLeft();
		if (noOfDays <= 10) {
			if (noOfDays <= daysLeft) {
				int leavesLeft =this.withdrawLeave(employeeID, noOfDays);
				return leavesLeft;
			}
			else
			{
				throw new LeavesNotAvailable("Sufficient Leaves not Appliable");
			}
		}
		throw new LeavesNotAvailable("Leaves above 10 are not available in a Single application.");
	}

	@Override
	public int withdrawLeave(long employeeID, int noOfDays) throws EmployeeNotFound{
		EmployeeManagement emp = this.dao.fetchEmployeeByID(employeeID);
		int daysLeft = this.dao.fetchEmployeeByID(employeeID).getLeavesLeft() - noOfDays;
		emp.setLeavesLeft(daysLeft);
		this.dao.updateEmployee(emp, employeeID);
		return daysLeft;
	}

}
