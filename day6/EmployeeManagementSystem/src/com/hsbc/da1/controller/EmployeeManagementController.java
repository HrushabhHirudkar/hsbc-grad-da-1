package com.hsbc.da1.controller;

import java.util.List;

import com.hsbc.da1.exceptions.EmployeeNotFound;
import com.hsbc.da1.exceptions.LeavesNotAvailable;
import com.hsbc.da1.model.EmployeeManagement;
import com.hsbc.da1.service.EmployeeManagementService;
import com.hsbc.da1.service.EmployeeManagementServiceImpl;

public class EmployeeManagementController {

	private EmployeeManagementService service = new EmployeeManagementServiceImpl();
	
	public EmployeeManagement addEmployee(String name, int age, double salary) {
		return this.service.addEmployee(name, age, salary);
	}

	public void deleteEmployee(long employeeID) {
		this.service.deleteEmployee(employeeID);
	}

	public List<EmployeeManagement> fetchEmployee() {
		return this.service.fetchEmployee();
	}

	public EmployeeManagement fetchEmployeeByID(long employeeID) throws EmployeeNotFound{
		return this.service.fetchEmployeeByID(employeeID);
	}

	public int applyforleave(long employeeID, int noOfDays) throws LeavesNotAvailable,EmployeeNotFound{
		return this.service.applyforleave(employeeID, noOfDays);
	}

	public void withdrawLeave(long employeeID, int noOfDays) throws EmployeeNotFound{
		this.service.withdrawLeave(employeeID, noOfDays);
	}
}
