package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hsbc.da1.model.Item;

public class ItemDAOImpl implements ItemDAO {

	private static final String SELECT_QUERY = "select * from items";
	private static final String INSERT_QUERY = "insert into items (name, price) values(?, ?)";

	private static String connectString = "jdbc:derby://localhost:1527/mydb";
	private static String username = "admin";
	private static String password = "password";

	static Connection getConnection() {
		try {
			Connection connection = DriverManager.getConnection(connectString, username, password);
			return connection;
		} catch (SQLException e) {
			System.out.println("Error");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Item saveItem(Item item) {
//		Connection connection = getConnection();
		try{
			Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/mydb","admin","password");
			System.out.println(connection);
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY);
			preparedStatement.setString(1, item.getName());
			preparedStatement.setDouble(2, item.getPrice());
			int rowsUpdated = preparedStatement.executeUpdate();
			if (rowsUpdated > 0)
				return item;
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Item not inserted");
		return item;
	}

	@Override
	public List<Item> fetchItems() {
		List<Item> items = new ArrayList<>();
//		Connection connection = getConnection();

		try(Connection connection = DriverManager.getConnection(connectString, username, password);) {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(SELECT_QUERY);
			while (rs.next()) {
				items.add(new Item(rs.getLong("itemId"), rs.getString("itemName"), rs.getDouble("price")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return items;
	}

}

// create table items(itemId bigint not null generated always as identity (start
// with 0, increment by 1), itemName varchar(10) not null, price decimal(10,2),
// constraint primary_key primary key(itemId));
