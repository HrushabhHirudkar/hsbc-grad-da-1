package com.hsbc.da1.model;

public class User {
	
	private String username;
	private int age;
	
	public User(String username,int age) {
		this.username = username;
		this.age = age;
	}

	public String getUsername() {
		return username;
	}

	public void setUserName(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [userName=" + username + ", age=" + age + "]";
	}
	
}
