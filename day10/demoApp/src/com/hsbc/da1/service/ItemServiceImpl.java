package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.dao.ItemDAO;
import com.hsbc.da1.model.Item;

public class ItemServiceImpl implements ItemService {

	private ItemDAO itemDao = ItemDAO.getInstance();

	@Override
	public Item saveItem(Item item) {
		return this.itemDao.saveItem(item);
	}

	@Override
	public List<Item> fetchItems() {
		return this.itemDao.fetchItems();
	}

}
