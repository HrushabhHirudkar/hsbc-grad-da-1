package com.hsbc.da1.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hsbc.da1.model.User;

public class GreetingServlet extends HttpServlet {
	@Override
	public void init() {
		System.out.println("Inside the init method.");
	}

	@Override
	public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws IOException, ServletException {

		
		String reqUsername = (String) httpServletRequest.getAttribute("username");
		String reqClass = (String) httpServletRequest.getAttribute("class");
		System.out.println("Req scope, Username : "+reqUsername);
		System.out.println("Req scope, class : "+reqClass);
		
		HttpSession session = httpServletRequest.getSession();
		
		System.out.println("================ Session Scope start =========================");
		
		User user = (User) session.getAttribute("user");
		System.out.println("Req scope, Username : "+reqUsername);
		System.out.println("Req scope, class : "+reqClass);
		System.out.println("User : "+user);
		
		System.out.println(" Updating the state of the user ");
		session.removeAttribute("user");
        session.setAttribute("user", new User("Kiran", 44));

		
		System.out.println("================ Session Scope close =========================");
		
		
		
//		String firstName = httpServletRequest.getParameter("firstName");
//		String lastName = httpServletRequest.getParameter("lastName");
//
//		LocalDateTime currentDate = LocalDateTime.now();
//
//		PrintWriter out = httpServletResponse.getWriter();
//
//		out.write("Welcome : " + firstName + " " + lastName + "<h1>" + currentDate.toString() + "</h1>");
	}
}
