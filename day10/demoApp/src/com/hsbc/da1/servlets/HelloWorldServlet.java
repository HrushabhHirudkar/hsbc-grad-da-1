package com.hsbc.da1.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hsbc.da1.model.User;

public class HelloWorldServlet extends HttpServlet {

	@Override
	public void init() {
		System.out.println("Inside the init method.");
	}

	@Override
	public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws IOException, ServletException {

		LocalDateTime currentDate = LocalDateTime.now();
		
//		User user = new User("Aman", 25);
//		
//		String flag = httpServletRequest.getParameter("flag");
//		
//		if(flag.equalsIgnoreCase("req")) {
//			httpServletRequest.setAttribute("username", "Aman");
//			httpServletRequest.setAttribute("class","Servlet and JSP");
//			httpServletRequest.setAttribute("user", user);
//			RequestDispatcher rd = httpServletRequest.getRequestDispatcher("/greet");
//			rd.forward(httpServletRequest, httpServletResponse);
//			
//		}
//		else if (flag.equalsIgnoreCase("session")) {
//			
//			HttpSession sessionScope = httpServletRequest.getSession();
//			sessionScope.setAttribute("username", "Raj");
//			sessionScope.setAttribute("class", "Java EE");
//			sessionScope.setAttribute("user", user);
//			
//		}else {
//			
//			ServletContext context = httpServletRequest.getServletContext();
//			context.setAttribute("username", "Gobal Declaration");
//			context.setAttribute("class", "JAVA");
//			
//		}
		
		
		
		
		

		PrintWriter out = httpServletResponse.getWriter();
//
//		if (httpServletRequest.getParameter("firstName") != null
//				&& httpServletRequest.getParameter("lastName") != null) {
//			RequestDispatcher rd = httpServletRequest.getRequestDispatcher("/greet");
//			rd.forward(httpServletRequest, httpServletResponse);
//		}
//		else {
//			httpServletResponse.sendRedirect("http://www.google.com");
//		}

		out.write("<h1>"+currentDate.toString()+"</h1>");
	}
}
