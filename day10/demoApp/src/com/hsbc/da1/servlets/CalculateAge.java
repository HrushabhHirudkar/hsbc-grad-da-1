package com.hsbc.da1.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.*;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.User;

public class CalculateAge extends HttpServlet {
	@Override
	public void init() {
		System.out.println("Inside the init method.");
	}

	@Override
	public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws IOException {

		User user = (User) httpServletRequest.getSession().getAttribute("user");

		System.out.println(" User details in Age Calculator servlet " + user);

//		String dobStr = httpServletRequest.getParameter("dob");

//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//		LocalDate dateOfBirth = LocalDate.parse(dobStr,formatter);
//		LocalDate currentDate = LocalDate.now();
//		
//		Period age = Period.between(dateOfBirth, currentDate);
//		int ageInDays = age.getYears()*365+age.getMonths()*31+age.getDays();
//		PrintWriter out = httpServletResponse.getWriter();
//		
//		out.write("The age is :"+age.getYears()+" Years "+age.getMonths()+" Months "+age.getDays()+" Days."+"<h1>"+currentDate.toString()+"</h1>");
//		out.write("The age in days is :"+ageInDays+" Days");
	}
}
