package com.hsbc.da1.servlets;


import javax.servlet.http.*;

import com.hsbc.da1.model.User;

public class LogoutServlet extends HttpServlet{

	@Override

	public void doGet(HttpServletRequest req, HttpServletResponse res) {

		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("user");

		System.out.println(" Logging out the user " + user.getUsername());
		session.invalidate();

	}

}
