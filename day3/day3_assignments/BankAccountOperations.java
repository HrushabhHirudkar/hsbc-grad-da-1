//PARENT CLASS
abstract class BankAccount{

    private static long accountNUmberTracker = 1000;
    private String name;
    private final long accountNumber;
    private double accountBalance;

    public BankAccount(String name,double accountBalance){
        accountNumber = ++accountNUmberTracker;
        this.name = name;
        this.accountBalance = accountBalance;
    }

    public abstract void applyForLoan();
    public abstract void minBalance();
    public abstract void withdrawAmount(double amount);
    public abstract void operation(double addMoney);
    public double getAccountBalance(){
        return accountBalance;
    }
    public double completeTransaction(double amount){
        accountBalance=accountBalance-amount;
        return accountBalance;
    }
    public final void depositMoney(double bal){
        System.out.println("Depositing Money : ");
        this.accountBalance = accountBalance + bal;
    }
    public final void showData(){
        System.out.println("The name of account holder is  :"+name);
        System.out.println("The account balance is : "+ accountBalance);
        System.out.println("The account number is : "+ accountNumber);
    }
    public final void addLoanMoney(double LOAN_AMOUNT){
        this.accountBalance=accountBalance+LOAN_AMOUNT;
    }

}

//CHILD CLASS CURRENT ACCOUNT

final class CurrentAccount extends BankAccount{
    public CurrentAccount(String name,double accountBalance){
        super(name,accountBalance);
    }
    private final double LOAN_AMOUNT = 2500000;
    @Override
    public final void operation(double addMoney) {
        // TODO Auto-generated method stub
        System.out.println("*****************\nBefore any Transactions : \n **********************");
        super.showData();
        System.out.println("*****************\nAfter the Transactions : \n ***********************");
        super.depositMoney(addMoney);
        super.showData();
        System.out.println("*********************");
        minBalance();
        withdrawAmount(80000);
        applyForLoan();
        System.out.println("*********************");
        super.showData();
    }
    @Override
    public final void applyForLoan(){
        System.out.println("You can only apply for 25 lakhs.");
        super.addLoanMoney(LOAN_AMOUNT);
    }
    @Override
    public final void minBalance()
    {
        if(super.getAccountBalance()<25000)
        {
            System.out.println("Your account does not have the minimum balance required i.e. 25000");
        }
        else{
            System.out.println("Account balance is "+ super.getAccountBalance());
        }
    }
    @Override
    public final void withdrawAmount(double amount){
        System.out.println("Withdrawing amount : "+ amount);
        if(amount>0){
            if((super.getAccountBalance()-amount)>25000){
                System.out.println("Withdraw Successfull of "+ amount);
                super.completeTransaction(amount);
                System.out.println("Account balance is "+ super.getAccountBalance());
            }
            else{
                System.out.println("You cannot withdraw this amount as your bank balance will be below the required amount.");
            }
        }
        else
        {
            System.out.println("You cannot withdraw this amount as it is larger than the acceptable value.");
        }
    }

}

//CHILD CLASS SAVINGS ACCOUNT

final class SavingsAccount extends BankAccount{
    public SavingsAccount(String name,double accountBalance){
        super(name,accountBalance);
    }
    private final double LOAN_AMOUNT = 500000;
    @Override
    public final void operation(double addMoney) {
        // TODO Auto-generated method stub
        System.out.println("*****************\nBefore any Transactions : \n **********************");
        super.showData();
        System.out.println("*****************\nAfter the Transactions : \n ***********************");
        super.depositMoney(addMoney);
        super.showData();
        System.out.println("*********************");
        minBalance();
        withdrawAmount(80000);
        applyForLoan();
        System.out.println("*********************");
        super.showData();
    }
    @Override
    public final void applyForLoan(){
        System.out.println("You can only apply for 5 lakhs.");
        super.addLoanMoney(LOAN_AMOUNT);
    }
    @Override
    public final void minBalance()
    {
        if(super.getAccountBalance()<10000)
        {
            System.out.println("Your account does not have the minimum balance required i.e. 10000");
        }
        else{
            System.out.println("Account balance is "+ super.getAccountBalance());
        }
    }
    @Override
    public final void withdrawAmount(double amount){
        System.out.println("Withdrawing amount : "+ amount);
        if(amount<=10000){
            if((super.getAccountBalance()-amount)>10000){
                System.out.println("Withdraw Successfull of "+ amount);
                super.completeTransaction(amount);
                System.out.println("Account balance is "+ super.getAccountBalance());
            }
            else{
                System.out.println("You cannot WithDraw the amount "+ amount);
            }
        }
        else
        {
            System.out.println("You cannot withdraw this amount as it is larger than the acceptable value.");
        }
    }
}

//CHILD CLASS SALARIED ACCOUNT

final class SalariedAccount extends BankAccount{
    public SalariedAccount(String name,double accountBalance){
        super(name,accountBalance);
    }
    private final double LOAN_AMOUNT = 1000000;
    @Override
    public final void operation(double addMoney) {
        // TODO Auto-generated method stub
        System.out.println("*****************\nBefore any Transactions : \n **********************");
        super.showData();
        System.out.println("*****************\nAfter the Transactions : \n ***********************");
        super.depositMoney(addMoney);
        super.showData();
        System.out.println("*********************");
        minBalance();
        withdrawAmount(10000);
        applyForLoan();
        System.out.println("*********************");
        super.showData();
    }
    @Override
    public final void applyForLoan(){
        System.out.println("You can only apply for 10 lakhs.");
        super.addLoanMoney(LOAN_AMOUNT);
    }
    @Override
    public final void minBalance()
    {
        if(super.getAccountBalance()<0)
        {
            System.out.println("Your account does not have the minimum balance required i.e. 0");
        }
        else{
            System.out.println("Account balance is "+ super.getAccountBalance());
        }
    }
    @Override
    public final void withdrawAmount(double amount){
        System.out.println("Withdrawing amount : "+ amount);
        if(amount<=15000){
            if((super.getAccountBalance()-amount)>0){
                System.out.println("Withdraw Successfull of "+ amount);
                super.completeTransaction(amount);
                System.out.println("Account balance is "+ super.getAccountBalance());
            }
            else{
                System.out.println("You cannot withdraw this amount as your bank balance will be below the required amount.");
            }
        }
        else
        {
            System.out.println("You cannot withdraw this amount as it is larger than the acceptable value.");
        }
    }
}

public class BankAccountOperations {
    public static void main(String[] args) {
        int accountType = Integer.parseInt(args[0]);
        double addMoney = 50000;

        BankAccount account = null;

        switch (accountType) {
            case 1:
            account = new CurrentAccount("Aman",30000);
                break;
            case 2:
            account = new SavingsAccount("Raj",40000);
                break;
            case 3:
            account = new SalariedAccount("Ram",0);
                break;
        
            default:
                System.out.println("Invalid input.\n");
                break;
        }

        System.out.println("***************************");

        execute(account,addMoney);

        
    }
    public static void execute(BankAccount account,Double addMoney) {
        account.operation(addMoney);
    }

}
