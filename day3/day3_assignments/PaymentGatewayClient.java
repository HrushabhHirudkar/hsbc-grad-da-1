//Interface are abstract by nature
interface PaymentGateway{
    
    void pay(String from, String to,double amount,String message);
}

interface MobileRecharge{

    void recharge(String phoneNo, double amount);
}

//class can implement any number of methods
//untill all the methods of interfaces are overriden in a class it is called as abstract
class GooglePay implements PaymentGateway,MobileRecharge{

    public void pay(String from, String to, double amount, String message){
        System.out.println("Amount : " + amount + " is being sent from "+ from +" to " + to +"\n. Message : "+message+ "\n VIA GOOGLE PAY");
    }

    public void recharge(String phoneNo, double amount){
        System.out.println("Completing mobile recharge for mobile number " + phoneNo + " of amount "+ amount+ "\n VIA GOOGLE PAY");
    }

}

class PhonePay implements PaymentGateway,MobileRecharge{
    public void pay(String from, String to, double amount, String message){
        System.out.println("Amount : "+amount+" is being sent from "+from+" to " +to +"\n. Message : "+message+ "\n VIA PHONE PAY");
    }   

    public void recharge(String phoneNo, double amount){
        System.out.println("Completing mobile recharge for mobile number " + phoneNo + " of amount "+ amount+ "\n VIA PHONE PAY");
    }
}

class JioPay implements PaymentGateway{
    public void pay(String from, String to, double amount, String message){
        System.out.println("Amount : "+amount+" is being sent from "+from+" to "+ to +"\n. Message : "+message+ "\n VIA JIO PAY");
    }
    
}

public class PaymentGatewayClient {
    public static void main(String[] args) {
        
        PaymentGateway paymentGateway = null;
        MobileRecharge mobileRecharge = null;

        int inputType = Integer.parseInt(args[0]);

        switch (inputType) {
            case 1:
                GooglePay gPay = new GooglePay();
                paymentGateway = gPay;
                mobileRecharge = gPay;
                break;
            case 2:
                PhonePay phonePay = new PhonePay();
                paymentGateway = phonePay;
                mobileRecharge = phonePay;
                break;
        
            case 3:
                JioPay jPay = new JioPay();
                paymentGateway = jPay;
                System.out.println("Jio Pay cannot perform mobile recharges.");
                break;
        
            default:
                System.out.println("Please enter a correct choice.");
                break;
        }

        paymentGateway.pay("Aman", "Raj", 10_000, "Please Respond");
        if(inputType!=3){
        mobileRecharge.recharge("7410852953", 500);
        }


    }
}
