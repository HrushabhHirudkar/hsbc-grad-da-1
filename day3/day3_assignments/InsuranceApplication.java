interface Insurance{
    double calculatePremium(double insuranceAmount, String carName, String carModel);

    String payPremium(double premiumAmount, String carName,String carNumber, String carModel);

}

class BajajInsurance implements Insurance{
    private static long amountOfPolicies = 1000;
    private static double PERCENTAGE = 0.2;

    public double calculatePremium(double insuranceAmount, String carName, String carModel){

        return PERCENTAGE*insuranceAmount;
    }

    public String payPremium(double premiumAmount, String carName,String carNumber, String carModel){

        ++amountOfPolicies;
        return "BajajInsurance_"+carName+"_"+carNumber+"_"+carModel+"_"+amountOfPolicies;
    }
}

class TataAig implements Insurance{
    private static long amountOfPolicies = 1000;
    private static double PERCENTAGE1 = 0.15;
    private static double PERCENTAGE2 = 0.1;
    
    public double calculatePremium(double insuranceAmount, String carName, String carModel){

        if(insuranceAmount>=1000000){
            return PERCENTAGE1*insuranceAmount;
        }
        else{
            return PERCENTAGE2*insuranceAmount;
        }
    }

    public String payPremium(double premiumAmount, String carName,String carNumber, String carModel){

        ++amountOfPolicies;
        return "TataAig_"+carName+"_"+carNumber+"_"+carModel+"_"+amountOfPolicies;
    }
}

class Account{
    private String name;
    private double accountBalance;

    public Account(String name, double accountBalance){
        this.name=name;
        this.accountBalance = accountBalance;
    }

    public double getAccountBalance(){
        return this.accountBalance;
    }

    public void withdrawAmount(Double amount){
        accountBalance = accountBalance - amount;
    }

    public void showData(){
        System.out.println("Account Holder name : " + name);
        System.out.println("Account Balance : "+ accountBalance );
    }
}

public class InsuranceApplication {
    public static void main(String[] args) {
        int choiceOfBank = Integer.parseInt(args[0]);
        Insurance insurance = null;
        Account account =new Account("Aman", 200000);

        switch (choiceOfBank) {
            case 1:
                BajajInsurance bInsurance = new BajajInsurance();
                insurance = bInsurance;
                
                break;
            case 2:
                TataAig tataAig = new TataAig();
                insurance = tataAig;

                break;
            default:
                System.out.println("Please enter a valid option.");
                break;
        }

        double premiumAmount=insurance.calculatePremium(600000, "Swift", "2017");
        System.out.println("The premium amount is : "+premiumAmount);
        System.out.println("Bank Account Balance : "+ account.getAccountBalance());
        if(account.getAccountBalance()>premiumAmount){
            String policyNumber=insurance.payPremium(premiumAmount, "Swift","ABC1234", "2017");
            account.withdrawAmount(premiumAmount);
            System.out.println("================");
            account.showData();
            System.out.println("================");
            System.out.println("Your policy number is : "+ policyNumber);
        }
        else
        {
            System.out.println("You do not have sufficient balance for the insurance. \n Account Balance is : "+account.getAccountBalance());
        }
    }
}
