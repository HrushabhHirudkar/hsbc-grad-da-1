
public class BillAfterTax {
    public static void main(String[] args) {
        String value = args[0];
        float billAmount = Float.parseFloat(value);
        
        //Code for 3
        /*
        billAmount= (float) (1.15 * billAmount);
        */

        //Here starts the code for 3.1
        String statecode = args[1];

        if(statecode.equals("KA"))
        {
            billAmount= (float) (1.15 * billAmount);
        }
        else if(statecode.equals("TN")){
            billAmount= (float) (1.18 * billAmount);
        }
        else if(statecode.equals("MH")){
            billAmount= (float) (1.20 * billAmount);
        }
        else{
            billAmount= (float) (1.12 * billAmount);
        }
        

        System.out.println("The bill after taxes is : "+ billAmount);

    }  
}