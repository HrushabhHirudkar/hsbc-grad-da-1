public class SavingsAccountRegister{

    private static int userNumber = 0;
    private static Account[] accountRegister = new Account[2];

    public void addMember(Account user){
        accountRegister[userNumber]=user;
        userNumber++;
    }

    public static Account fetchUserAccountDetails(long accountNumber){
        for (Account accountfind : accountRegister) {
            if (accountfind.getAccountNo() == accountNumber) {
                return accountfind;
            }
        }
        return null;
    }
}