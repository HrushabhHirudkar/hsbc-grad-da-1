class Account{

    private static long accountNumberTracker = 10_000;
    
    private long accountNumber;
    
    private double accoutBalance;
    
    private String gstNumber;
    
    private String customerName,businessName;

    private Address address;
    
    //private String street;

    //private String city;

    //private int zipCodes;

    //private String address;
    
    public Account(String customerName, String businessName, String gstNumber, Address address){
        this.customerName = customerName;
        this.businessName = businessName;
        this.gstNumber = gstNumber;
        this.address = address;
        this.accountNumber = ++ accountNumberTracker;
        this.accoutBalance = 50_000;
    }
    public Account(String customerName, String businessName, String gstNumber){
        this.customerName = customerName;
        this.businessName = businessName;
        this.gstNumber = gstNumber;
        this.accountNumber = ++ accountNumberTracker;
        this.accoutBalance = 50_000;
    }
    public Account(String customerName, String businessName, String gstNumber, double accoutBalance){
        this.customerName = customerName;
        this.businessName = businessName;
        this.gstNumber = gstNumber;
        this.accountNumber = ++ accountNumberTracker;
        this.accoutBalance = accoutBalance;
    }
    public Account(String customerName, String businessName, String gstNumber, Address address, double accoutBalance){
        this.customerName = customerName;
        this.businessName = businessName;
        this.gstNumber = gstNumber;
        this.address = address;
        this.accountNumber = ++ accountNumberTracker;
        this.accoutBalance = accoutBalance;
    }

    public double getBalance(){
        return this.accoutBalance;
    }

    public Boolean checkBalance(){
        if(getBalance()<50_000){
            System.out.println("You DO NOT have the minimum balance required.");
            return false;
        }
        else{
            return true;
        }
    }

    public String getCustName()
    {
        return this.customerName;
    }
    public String getBusinessName()
    {
        return this.businessName;
    }
    public String getGstNo()
    {
        return this.gstNumber;
    }
   /* public String getAddress()
    {
        address = "Street : "+this.street +" || City : "+ this.city +" || ZipCode : "+ this.zipCodes;
        return address;
    }*/
    public long getAccountNo()
    {
        return this.accountNumber;
    }

    public void updateAddress(Address address){
        this.address=address;
    }

    public void showDetails(){
        System.out.println("Customer name : "+ getCustName());
        System.out.println("Business name : "+ getBusinessName());
        System.out.println("GST number : "+ getGstNo());
        System.out.println("Address : "+ address.getAddress());
        System.out.println("Account number : "+ getAccountNo());
        System.out.println("Account Balance : "+ getBalance());
    }

    public double depositAmount(double amount){
        this.accoutBalance = accoutBalance + amount;
        return accoutBalance;
    }

    public double withdrawAmount(double amount){
        this.accoutBalance = accoutBalance - amount;
        if(accoutBalance>=50_000)
        {
            return accoutBalance;
        }
        else{
            System.out.println("You cannot WithDraw this amount."+ amount);
            accoutBalance = accoutBalance + amount;
            return accoutBalance;
        }
    }

    //Transfer Amount
    public void transferAmount(double amount, Account user){
        //validation
        if(amount>this.accoutBalance){
            System.out.println("You cannot Transfer this large amount.\n Please check the amount entered.");
        }
        else{
            //withdraw from current account
            withdrawAmount(amount);
            //deposit to other account
            user.depositAmount(amount);
        }
    }

    //Transfer Amount using AccountID
    public void transferAmount(double amount, long userAccountNumber){
        Account user = SavingsAccountRegister.fetchUserAccountDetails(userAccountNumber);
        if(user == null){
            System.out.println("The provided Account Number does not exist.");
        }
        else{
            if(amount>this.accoutBalance){
                System.out.println("You cannot Transfer this large amount.\n Please check the amount entered.");
            }
            else{
                //withdraw from current account
                withdrawAmount(amount);
                //deposit to other account
                user.depositAmount(amount);
            }
        }
    }

}

public class CurrentAccount{
    public static void main(String[] args) {
        SavingsAccountRegister savingsRegister = new SavingsAccountRegister();
        Address address = new Address("8th Ave", "Pune", "maharashtra", 411046);
        Account aman = new Account("Aman","Aman Enterprises","ABCD1234",address);
        savingsRegister.addMember(aman);
        aman.showDetails();
        aman.withdrawAmount(80000);
        System.out.println("Account Balance : "+ aman.getBalance());
        aman.depositAmount(100000);
        System.out.println("Account Balance : "+ aman.getBalance());
        aman.withdrawAmount(80000);
        System.out.println("Account Balance : "+ aman.getBalance());

        System.out.println("================================");

         Address address_raj = new Address("10th Ave", "Pune", "maharashtra", 411044);
         Account raj = new Account("Raj","Raj Technologies", "PQRS7894" , address_raj);
         savingsRegister.addMember(raj);
         raj.showDetails();
         System.out.println("================================");
         aman.transferAmount(20000,10003);
         System.out.println("================================");
        aman.showDetails();
         System.out.println("================================");
        raj.showDetails();

    }
}