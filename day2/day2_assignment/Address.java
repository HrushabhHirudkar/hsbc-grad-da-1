public class Address{

    private String city;
    private String street;
    private int zipCode;
    private String state;
    private String address;

    public Address(String street, String city,String state,int zipCode){
        this.state=state;
        this.street=street;
        this.zipCode=zipCode;
        this.city=city;
    }

    public Address(){}

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress()
    {
        address = "Street : "+this.street +" || City : "+ this.city +" || ZipCode : "+ this.zipCode+ " || State : "+this.state;
        return address;
    }

}