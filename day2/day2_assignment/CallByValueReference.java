public class CallByValueReference{

    public static void main(String[] args) {
        int operand1 = 10;
        int operand2 = 20;
        //Call By Value
        System.out.println("====================================");
        System.out.println("====================================");
        System.out.println("Before Call by Value : ");
        System.out.println("operand 1 = "+operand1+ " and Operand 2 = "+operand2);
        callByVal(operand1,operand2);
        System.out.println("After Call by Value : ");
        System.out.println("operand 1 = "+operand1+ " and Operand 2 = "+operand2);
        System.out.println("====================================");

        System.out.println("**************************************");
        //Call By Reference
        int array[] = new int[] {10,20,30,40};
        System.out.println("====================================");
        System.out.println("Before Call by Reference : ");
        for(int a : array){
            System.out.println(a);
        }
        callByRef(array);
        System.out.println("After Call by Reference : ");
        for(int a : array){
            System.out.println(a);
        }
        System.out.println("====================================");
    }

    private static void callByVal(int operand1, int operand2) {
        operand1 = operand1*10;
        operand2 = operand2*10;

        System.out.println("Inside Call by Value : ");
        System.out.println("operand 1 = "+operand1+ " and Operand 2 = "+operand2);
    }

    private static void callByRef(int [] array){
        array[0]=array[0]*10;
        array[1]=array[1]*10;
        array[2]=array[2]*10;
        array[3]=array[3]*10;

        System.out.println("Inside Call by Reference : ");
        for(int a : array){
            System.out.println(a);
        }
    }
}