package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.controller.SavingsAccountControllerImpl;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import java.util.*;

public class SavingsAccountClient {

	public static void main(String[] args) {
		SavingsAccountController savingsaccountController = new SavingsAccountControllerImpl();

		SavingsAccount amanSavingsAccount = savingsaccountController.openSavingsAccount("Aman", 30_000, "8th Avenue",
				"Pune", 411046);
		SavingsAccount rajSavingsAccount = savingsaccountController.openSavingsAccount("Raj", 50_000);

		System.out.println("Account number : " + amanSavingsAccount.getAccountNumber());
		System.out.println("Account number : " + rajSavingsAccount.getAccountNumber());

		try {
			savingsaccountController.transfer(amanSavingsAccount.getAccountNumber(),
					rajSavingsAccount.getAccountNumber(), 10_000);
		} catch (InsufficientBalanceException exception) {
			System.out.println(exception.getMessage());
		} catch (CustomerNotFoundException customerException) {
			System.out.println(customerException.getMessage());
		}

		List<SavingsAccount> account = savingsaccountController.fetchSavingsAccounts();

		for (SavingsAccount sa : account) {
			if (sa != null) {
				System.out.println("=====================================");
				System.out.println("Account name : " + sa.getCustomerName());
				System.out.println("Account number : " + sa.getAccountNumber());
				System.out.println("Account balance : " + sa.getAccountBalance());
				if (sa.getAddress() != null) {
					System.out.println("Street : " + sa.getAddress().getStreet() + " City : "
							+ sa.getAddress().getCity() + " ZipCode : " + sa.getAddress().getZipCode());
				}
			}
		}

	}
}
