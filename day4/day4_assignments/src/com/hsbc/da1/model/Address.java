package com.hsbc.da1.model;

public class Address {
	
	private String city;
	private String street;
	private long zipCode;
	
	public Address(String street, String city, long zipCode) {
		this.city = city;
		this.street =street;
		this.zipCode =zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public long getZipCode() {
		return zipCode;
	}

	public void setZipCode(long zipCode) {
		this.zipCode = zipCode;
	}
	
	
}
