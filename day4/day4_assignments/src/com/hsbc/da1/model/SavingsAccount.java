package com.hsbc.da1.model;

public class SavingsAccount {
	
	private String customerName;
	
	private long accountNumber;
	
	private double accountBalance;
	
	private Address address;
	
	private static long counter = 1000;
	
	public SavingsAccount(String customerName,double accountBalance) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++counter;
	}
	
	public Address getAddress() {
		//System.out.println(address.getCity() + " "+ address.getStreet() + " "+ address.getZipCode());
			return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public SavingsAccount(String customerName,double accountBalance, Address address) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++counter;
		this.address = address;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	
	
}