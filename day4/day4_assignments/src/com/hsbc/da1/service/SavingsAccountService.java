package com.hsbc.da1.service;

import com.hsbc.da1.dao.*;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.*;
import java.util.*;

public interface SavingsAccountService {

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance);

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String street, String city,
			long zipCode);

	public void deleteSavingsAccount(long accountNumber);

	public List<SavingsAccount> fetchSavingsAccounts();

	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFoundException;

	public double checkBalance(long accountNumber) throws CustomerNotFoundException;

	public double depositAmount(long accountNumber, double amount) throws CustomerNotFoundException;

	public double withdrawAmount(long accountNumber, double amount)
			throws InsufficientBalanceException, CustomerNotFoundException;

	public void transfer(long fromID, long toID, double amount)
			throws InsufficientBalanceException, CustomerNotFoundException;

}
