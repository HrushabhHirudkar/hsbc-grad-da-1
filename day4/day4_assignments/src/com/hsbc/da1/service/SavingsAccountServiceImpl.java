package com.hsbc.da1.service;

import com.hsbc.da1.dao.ArrayBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.*;
import com.hsbc.da1.util.SavingsAccountDAOFactory;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

import java.util.*;

public class SavingsAccountServiceImpl implements SavingsAccountService {

	private SavingsAccountDAO dao = SavingsAccountDAOFactory.getsavingsAccountDAO(3);// IS-A implementataion ABSADAOIpl
																						// is a
																						// SavingsAccountDAo

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance) {

		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		dao.createSavingsAccount(savingsAccount);
		return savingsAccount;
	}

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String street, String city,
			long zipCode) {
		Address address = new Address(street, city, zipCode);
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance, address);
		dao.createSavingsAccount(savingsAccount);
		return savingsAccount;
	}

	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);
	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		List<SavingsAccount> account = dao.fetchSavingsAccounts();
		return account;
	}

	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByID(accountNumber);
		return savingsAccount;
	}

	public double checkBalance(long accountNumber) throws CustomerNotFoundException {
		SavingsAccount savingsAccount = this.fetchSavingsAccountByID(accountNumber);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	public double depositAmount(long accountNumber, double amount) throws CustomerNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByID(accountNumber);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.dao.updateSavingsAccount(accountNumber, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	public double withdrawAmount(long accountNumber, double amount)
			throws InsufficientBalanceException, CustomerNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByID(accountNumber);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if (currentAccountBalance >= amount) {
				currentAccountBalance = currentAccountBalance - amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountNumber, savingsAccount);
				return amount;
			} else {
				throw new InsufficientBalanceException("Do not have Sufficient Balance");
			}
		}
		return 0;
	}

	public void transfer(long fromID, long toID, double amount)
			throws InsufficientBalanceException, CustomerNotFoundException {
		SavingsAccount fromAccount = this.dao.fetchSavingsAccountByID(fromID);
		SavingsAccount toAccount = this.dao.fetchSavingsAccountByID(toID);
		double updatedBalance = this.withdrawAmount(fromAccount.getAccountNumber(), amount);
		if (updatedBalance != 0) {
			this.depositAmount(toAccount.getAccountNumber(), amount);
		}

	}

}
