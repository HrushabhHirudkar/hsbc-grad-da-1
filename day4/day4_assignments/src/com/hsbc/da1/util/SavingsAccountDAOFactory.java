package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArrayBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.ArrayListBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.LinkedListBackedSavingsAccountDOAImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;

public class SavingsAccountDAOFactory {

	public static SavingsAccountDAO getsavingsAccountDAO(int value) {
		if (value == 1) {
			return new ArrayBackedSavingsAccountDAOImpl();
		} else if (value == 2) {
			return new ArrayListBackedSavingsAccountDAOImpl();
		} else {
			return new LinkedListBackedSavingsAccountDOAImpl();
		}
	}

}
