package com.hsbc.da1.dao;

import java.util.Arrays;
import java.util.*;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class ArrayBackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	private SavingsAccount savingsAccounts[] = new SavingsAccount[100];

	private static int counter;

	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		savingsAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}

	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {

		for (int index = 0; index < 1; index++) {
			if (savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = savingsAccount;
			}
		}
		return savingsAccount;
	}

	public void deleteSavingsAccount(long accountNumber) {
		for (int index = 0; index < savingsAccounts.length; index++) {
			if (savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = null;
				break;
			}
		}
	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		return Arrays.asList(savingsAccounts);
	}

	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFoundException {
		for (int index = 0; index < savingsAccounts.length; index++) {
			if (savingsAccounts[index] != null) {
				if (savingsAccounts[index].getAccountNumber() == accountNumber) {
					return savingsAccounts[index];
				}
			}
		}
		throw new CustomerNotFoundException("The account number does not belong to any Customer.");
	}
}
