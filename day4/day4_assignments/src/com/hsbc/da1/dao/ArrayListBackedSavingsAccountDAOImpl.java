package com.hsbc.da1.dao;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;
import java.util.*;

public class ArrayListBackedSavingsAccountDAOImpl implements SavingsAccountDAO {

	private List<SavingsAccount> savingsAccountList = new ArrayList<>();

	@Override
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		System.out.println("Done using Array List");
		this.savingsAccountList.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for (SavingsAccount sa : savingsAccountList) {
			if (sa.getAccountNumber() == accountNumber) {
				sa = savingsAccount;
			}
		}
		return savingsAccount;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		for (SavingsAccount sa : savingsAccountList) {
			if (sa.getAccountNumber() == accountNumber) {
				this.savingsAccountList.remove(sa);
			}
		}
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return this.savingsAccountList;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		for (SavingsAccount sa : savingsAccountList) {
			if (sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
		throw new CustomerNotFoundException("The account number does not belong to any Customer.");
	}

}
