package com.hsbc.da1.dao;

import java.util.*;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.*;

public interface SavingsAccountDAO {
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount);

	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);

	public void deleteSavingsAccount(long accountNumber);

	public List<SavingsAccount> fetchSavingsAccounts();

	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFoundException;
}
