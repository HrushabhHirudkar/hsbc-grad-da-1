package com.hsbc.da1.dao;

import java.util.LinkedList;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class LinkedListBackedSavingsAccountDOAImpl implements SavingsAccountDAO {

	private List<SavingsAccount> savingsAccountLinkedList = new LinkedList<>();

	@Override
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		System.out.println("Done using Linked List.");
		this.savingsAccountLinkedList.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		for (SavingsAccount sa : savingsAccountLinkedList) {
			if (sa.getAccountNumber() == accountNumber) {
				savingsAccount = sa;
			}
		}
		return savingsAccount;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		for (SavingsAccount sa : savingsAccountLinkedList) {
			if (sa.getAccountNumber() == accountNumber) {
				this.savingsAccountLinkedList.remove(sa);
			}
		}

	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return this.savingsAccountLinkedList;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFoundException {
		for (SavingsAccount sa : savingsAccountLinkedList) {
			if (sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
		throw new CustomerNotFoundException("The account number does not belong to any Customer.");
	}

}
