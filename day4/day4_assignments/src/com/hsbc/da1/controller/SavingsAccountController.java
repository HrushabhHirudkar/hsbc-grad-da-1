package com.hsbc.da1.controller;

import com.hsbc.da1.exception.CustomerNotFoundException;
import java.util.*;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpl;

public interface SavingsAccountController {

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance);

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance, String street, String city,
			long zipCode);

	public void deleteSavingsAccount(long accountNumber);

	public List<SavingsAccount> fetchSavingsAccounts();

	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFoundException;

	public double checkBalance(long accountNumber) throws CustomerNotFoundException;

	public double depositAmount(long accountNumber, double amount) throws CustomerNotFoundException;

	public double withdrawAmount(int accountNumber, int amount)
			throws InsufficientBalanceException, CustomerNotFoundException;

	public void transfer(long fromID, long toID, double amount)
			throws InsufficientBalanceException, CustomerNotFoundException;

}
