package com.hsbc.da1.controller;

import com.hsbc.da1.exception.CustomerNotFoundException;
import java.util.*;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpl;

public class SavingsAccountControllerImpl implements SavingsAccountController {
	private SavingsAccountService savingsAccountService = new SavingsAccountServiceImpl();

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, accountBalance);
		return savingsAccount;
	}

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance, String street, String city,
			long zipCode) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, accountBalance,
				street, city, zipCode);
		return savingsAccount;
	}

	public void deleteSavingsAccount(long accountNumber) {
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		List<SavingsAccount> account = this.savingsAccountService.fetchSavingsAccounts();
		return account;
	}

	public SavingsAccount fetchSavingsAccountByID(long accountNumber) throws CustomerNotFoundException {
		SavingsAccount account = this.savingsAccountService.fetchSavingsAccountByID(accountNumber);
		return account;
	}

	public double checkBalance(long accountNumber) throws CustomerNotFoundException {
		return this.savingsAccountService.checkBalance(accountNumber);
	}

	public double depositAmount(long accountNumber, double amount) throws CustomerNotFoundException {
		return this.savingsAccountService.depositAmount(accountNumber, amount);
	}

	public double withdrawAmount(int accountNumber, int amount)
			throws InsufficientBalanceException, CustomerNotFoundException {
		return this.savingsAccountService.withdrawAmount(accountNumber, amount);
	}

	public void transfer(long fromID, long toID, double amount)
			throws InsufficientBalanceException, CustomerNotFoundException {
		this.savingsAccountService.transfer(fromID, toID, amount);
	}
}
