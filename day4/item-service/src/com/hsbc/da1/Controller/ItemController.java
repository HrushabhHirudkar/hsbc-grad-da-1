package com.hsbc.da1.Controller;

import com.hsbc.da1.Services.ItemService;
import com.hsbc.da1.Services.ItemServiceImpl;
import com.hsbc.da1.model.Item;
import com.hsbc.da1.util.ItemServiceFactory;

public class ItemController {
	
	private ItemService itemService = ItemServiceFactory.getItemService();
	
	public Item addItem(String itemName, double itemCost) {
		return this.itemService.addItem(itemName, itemCost);
	}

	public void deleteItem(long itemID){
		this.itemService.deleteItem(itemID);
	}

	public Item findItem(long itemID){
		return this.itemService.findItem(itemID);
	}
	
	public Item updateItemCost(long itemID, long newCost){
		return this.itemService.updateItemCost(itemID, newCost);
	}
	
	public Item findItemByID(long itemID){
		return this.itemService.findItemByID(itemID);
	}
	
	public Item[] fetchItem() {
		return this.itemService.fetchItem();
	}

}
