package com.hsbc.da1.Services;

import com.hsbc.da1.model.Item;

public interface ItemService {
	public Item addItem(String itemName, double itemCost);

	public void deleteItem(long itemID);

	public Item findItem(long itemID);

	public Item updateItemCost(long itemID, long newCost);
	
	public Item findItemByID(long itemID);
	
	public Item[] fetchItem();
}
