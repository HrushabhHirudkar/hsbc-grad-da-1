package com.hsbc.da1.Services;

import com.hsbc.da1.DOA.ItemDOA;
import com.hsbc.da1.DOA.ItemDOAImpl;
import com.hsbc.da1.util.ItemDOAFactory;
import com.hsbc.da1.model.Item;

public class ItemServiceImpl implements ItemService{

	private ItemDOA doa = ItemDOAFactory.getItemDOA();
	@Override
	public Item addItem(String itemName,double itemCost) {
		Item item = new Item(itemName, itemCost);
		this.doa.addItem(item);
		return item;
	}

	@Override
	public void deleteItem(long itemID) {
		this.doa.deleteItem(itemID);
		
	}

	@Override
	public Item findItem(long itemID) {
		Item item = this.doa.findItem(itemID);
		return item;
	}

	@Override
	public Item updateItemCost(long itemID, long newCost) {
		Item item  = this.findItemByID(itemID);
		if(item != null) {
			item.setItemCost(newCost);
			this.doa.updateItem(itemID, item);
		}
		return item;
	}

	@Override
	public Item findItemByID(long itemID) {
		Item item = this.doa.findItemByID(itemID);
		return item;
	}
	
	@Override
	public Item[] fetchItem() {
		return this.doa.fetchItem();
	}

}
