package com.hsbc.da1.DOA;

import com.hsbc.da1.model.Item;

public class ItemDOAImpl implements ItemDOA {

	private Item[] itemList = new Item[10];
	private static int counter = 0;

	@Override
	public Item addItem(Item item) {
		itemList[counter++] = item;
		return item;
	}

	@Override
	public void deleteItem(long itemID) {
		for (int index = 0; index < itemList.length; index++) {
			if (itemList[index].getItemID() == itemID) {
				itemList[index] = null;
			}
		}

	}

	@Override
	public Item findItem(long itemID) {
		for (int index = 0; index < counter; index++) {
			if (itemList[index].getItemID() == itemID) {
				return itemList[index];
			}
		}
		return null;
	}

	@Override
	public Item updateItem(long itemID, Item item) {
		for (int index = 0; index < itemList.length; index++) {
			if (itemList[index] != null) {
				if (itemList[index].getItemID() == itemID) {
					itemList[index] = item;
				}
			}
		}
		return item;
	}

	@Override
	public Item findItemByID(long itemID) {
		for (int index = 0; index < itemList.length; index++) {
			if (itemList[index].getItemID() == itemID) {
				return itemList[index];
			}
		}
		return null;
	}

	@Override
	public Item[] fetchItem() {
		return itemList;
	}

}
