package com.hsbc.da1.DOA;

import com.hsbc.da1.model.Item;

public interface ItemDOA {

	public Item addItem(Item item);

	public void deleteItem(long itemID);

	public Item findItem(long itemID);

	public Item updateItem(long itemID, Item item);
	
	public Item findItemByID(long itemID);
	
	public Item[] fetchItem();

}
