package com.hsbc.da1.util;

import com.hsbc.da1.DOA.ItemDOA;
import com.hsbc.da1.DOA.ItemDOAImpl;

public class ItemDOAFactory {
	
	public static ItemDOA getItemDOA(){
		ItemDOA itemDOA = new ItemDOAImpl();
		return itemDOA;
	}
}
