package com.hsbc.da1.util;

import com.hsbc.da1.Services.ItemService;
import com.hsbc.da1.Services.ItemServiceImpl;

public class ItemServiceFactory {
	
	public static ItemService getItemService() {
		return new ItemServiceImpl();
	}
}
