package com.hsbc.da1.Client;

import com.hsbc.da1.Controller.ItemController;
import com.hsbc.da1.model.Item;

public class ItemClient {

	public static void main(String[] args) {

		ItemController itemController = new ItemController();

		Item pen = itemController.addItem("Pen", 10);
		Item pencil = itemController.addItem("Pencil", 5);
		

		System.out.println("Item number of Pen :" + pen.getItemID());
		System.out.println("Item number of Pencil :" + pencil.getItemID());

		itemController.updateItemCost(pencil.getItemID(), 20);

		Item[] itemList = itemController.fetchItem();

		for (Item items : itemList) {
			if (items != null) {
				System.out.println("The item is : " + items);
			}
		}
	}

}
