package com.hsbc.da1.model;

public class Item {
	
	private String itemName;
	private double itemCost;
	private long itemID;
	private static long productIDcounter = 1000;
	
	public Item(String itemName,double itemCost) {
		this.itemName = itemName;
		this.itemCost = itemCost;
		this.itemID = ++productIDcounter;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getItemCost() {
		return itemCost;
	}

	public void setItemCost(double itemCost) {
		this.itemCost = itemCost;
	}

	@Override
	public String toString() {
		return "Item [itemName=" + itemName + ", itemCost=" + itemCost + ", itemID=" + itemID + "]";
	}

	public long getItemID() {
		return itemID;
	}

	public void setItemID(long itemID) {
		this.itemID = itemID;
	}
	
}
